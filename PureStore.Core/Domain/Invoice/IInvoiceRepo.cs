using System;

namespace PureStore.Core.Domain.Invoice
{
    public interface IInvoiceRepo
    {
        public void Save(Invoice invoice);
        public Invoice? FindById(Guid id);
    }
}