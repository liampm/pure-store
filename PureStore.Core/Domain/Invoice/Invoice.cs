using System;
using PureStore.Core.Domain.GiftCard;
using PureStore.Core.Domain.Offer;

namespace PureStore.Core.Domain.Invoice
{
    public record Invoice(
        Guid Id,
        Money SubTotal,
        Money Total,
        GiftVoucher[] GiftVouchers,
        OfferVoucher? OfferApplied,
        bool OfferUsed,
        string? Message);


}