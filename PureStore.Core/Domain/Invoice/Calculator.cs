using System;
using System.Linq;

namespace PureStore.Core.Domain.Invoice
{
    public static class Calculator
    {
        public static Invoice GenerateInvoice(Guid id, Basket.Basket basket)
        {
            var unchangeableTotal = basket.NonDiscountableTotal();
            var discountableTotal = basket.DiscountableTotal();

            // Sum all of the products
            var subTotal = unchangeableTotal.Add(discountableTotal);

            string? message = null;
            var offerSaving = new Money(0, "GBP");

            var offerUsed = false;
            if (basket.OfferVoucher != null)
            {
                Money? calculatedOfferSaving;
                (calculatedOfferSaving, message) = basket.OfferVoucher.CalculateSaving(
                    basket,
                    $"Offer Voucher {basket.OfferVoucher.Code}"
                );

                if (calculatedOfferSaving != null)
                {
                    offerSaving = (Money) calculatedOfferSaving;
                    offerUsed = true;
                }
            }

            var giftTotal = basket.GiftVouchers
                .Select(v => v.Amount)
                .Aggregate(new Money(0, "GBP"), (a, b) => a.Add(b));

            // unchangeable total + MAX((discountable total - offers - gift cards), 0)
            var finalTotal = unchangeableTotal.Add(discountableTotal.Sub(offerSaving).Sub(giftTotal));

            return new Invoice(id, subTotal, finalTotal, basket.GiftVouchers.ToArray(), basket.OfferVoucher, offerUsed, message);
        }
    }
}