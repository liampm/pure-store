namespace PureStore.Core.Domain.Basket
{
    public interface IBasketProvider
    {
        public Basket Current();
    }
}