using System;
using PureStore.Core.Domain.Product;

namespace PureStore.Core.Domain.Basket
{
    public class Item
    {
        private readonly Domain.Product.Product _product;
        public int Count { get; }

        public Item(Domain.Product.Product product, int count)
        {
            if (count < 1)
            {
                throw new ArgumentException("Cannot have less than 1 product as a basket item.");
            }

            _product = product;
            Count = count;
        }

        public Item Add()
        {
            return new(_product, Count + 1);
        }

        public Item Remove()
        {
            if (Count == 1)
            {
                throw new InvalidOperationException("Cannot have an empty basket item.");
            }

            return new Item(_product, Count - 1);
        }

        public Sku Sku()
        {
            return _product.Sku;
        }

        public string ProductName()
        {
            return _product.Name;
        }

        public Money UnitPrice()
        {
            return _product.Price;
        }

        public string Category()
        {
            return _product.Category;
        }

        public bool IsDiscountable()
        {
            return _product.Discountable;
        }
    }
}