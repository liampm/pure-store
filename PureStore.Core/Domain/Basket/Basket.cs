using System;
using System.Collections.Generic;
using System.Linq;
using PureStore.Core.Domain.GiftCard;
using PureStore.Core.Domain.Offer;

namespace PureStore.Core.Domain.Basket
{
    public class Basket
    {
        private readonly Dictionary<string, Item> _items;
        private readonly Dictionary<string, GiftVoucher> _giftVouchers;
        public IEnumerable<GiftVoucher> GiftVouchers => _giftVouchers.Values;
        public OfferVoucher? OfferVoucher { get; private set; }
        public Basket()
        {
            _items = new Dictionary<string, Item>();
            _giftVouchers = new Dictionary<string, GiftVoucher>();
            OfferVoucher = null;
        }

        public void AddProduct(Domain.Product.Product product)
        {
            if (_items.ContainsKey(product.Sku.ToString()))
            {
                _items[product.Sku.ToString()] = _items[product.Sku.ToString()].Add();
                return;
            }

            _items.Add(product.Sku.ToString(), new Item(product, 1));
        }

        public void RemoveProduct(Domain.Product.Product product)
        {
            if (!_items.ContainsKey(product.Sku.ToString()))
            {
                return;
            }

            try
            {
                _items[product.Sku.ToString()] = _items[product.Sku.ToString()].Remove();
            }
            catch (InvalidOperationException)
            {
                _items.Remove(product.Sku.ToString());
            }
        }

        public void AddGiftVoucher(GiftVoucher voucher)
        {
            if (_giftVouchers.ContainsKey(voucher.Code.ToString()))
            {
                return; // Ignore if already added.
            }

            _giftVouchers.Add(voucher.Code.ToString(), voucher);
        }

        public void RemoveGiftVoucher(GiftVoucher voucher)
        {
            if (!_giftVouchers.ContainsKey(voucher.Code.ToString()))
            {
                return;
            }

            _giftVouchers.Remove(voucher.Code.ToString());
        }

        public void ApplyOffer(OfferVoucher offer)
        {
            OfferVoucher = offer;
        }

        public void RemoveOffer()
        {
            OfferVoucher = null;
        }

        public Money DiscountableTotal()
        {
            return Total(i => i.IsDiscountable());
        }

        public Money NonDiscountableTotal()
        {
            return Total(i => !i.IsDiscountable());
        }

        public IEnumerable<Item> Items()
        {
            return _items.Values;
        }

        public IEnumerable<Item> DiscountableItems(Func<Item, bool> predicate)
        {
            return DiscountableItems().Where(predicate);
        }

        private Money Total(Func<Item, bool> predicate)
        {
            return _items.Values.Where(predicate)
                .Select(i => i.UnitPrice().Multiply(i.Count))
                .Aggregate(new Money(0, "GBP"), (a, b) => a.Add(b));
        }

        private IEnumerable<Item> DiscountableItems()
        {
            return _items.Values.Where(i => i.IsDiscountable());
        }
    }
}