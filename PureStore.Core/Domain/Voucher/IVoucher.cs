namespace PureStore.Core.Domain.Voucher
{
    public interface IVoucher
    {
        public (Money? saving, string? rejection) CalculateSaving(Basket.Basket basket, string context);
    }
}