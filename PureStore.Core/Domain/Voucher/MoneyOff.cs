using System;
using System.Linq;
using PureStore.Core.Domain.Basket;

namespace PureStore.Core.Domain.Voucher
{
    public class MoneyOff : IVoucher
    {
        private readonly Money _amount;
        private readonly Func<Item, bool> _itemFilter;

        public MoneyOff(Money amount, Func<Item, bool> itemFilter)
        {
            _amount = amount;
            _itemFilter = itemFilter;
        }

        public (Money? saving, string? rejection) CalculateSaving(Basket.Basket basket, string context)
        {
            var applicableItems = basket.DiscountableItems(_itemFilter).ToList(); // ToList to prevent enumeration

            if (!applicableItems.Any())
            {
                return (null, $"There are no products in your basket applicable to {context}.");
            }

            var applicableCost = applicableItems
                .Select(i => i.UnitPrice().Multiply(i.Count))
                .Aggregate((a, b) => a.Add(b));

            var saving = (applicableCost < _amount) ? applicableCost: _amount;

            return (saving, null);
        }
    }
}