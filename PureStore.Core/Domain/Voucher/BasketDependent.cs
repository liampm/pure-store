using System;

namespace PureStore.Core.Domain.Voucher
{
    public class BasketDependent : IVoucher
    {
        private readonly IVoucher _voucher;
        private readonly Func<Basket.Basket, string?> _basketCriteria;

        public BasketDependent(IVoucher voucher, Func<Basket.Basket, string?> basketCriteria)
        {
            _voucher = voucher;
            _basketCriteria = basketCriteria;
        }

        public (Money? saving, string? rejection) CalculateSaving(Basket.Basket basket, string context)
        {
            var rejection = _basketCriteria(basket);

            if (rejection != null)
            {
                return (null, rejection);
            }

            return _voucher.CalculateSaving(basket, context);
        }
    }
}