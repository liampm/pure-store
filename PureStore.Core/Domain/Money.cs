using System;

namespace PureStore.Core.Domain
{
    public readonly struct Money : IComparable<Money>
    {
        public decimal Amount { get; }
        private readonly string _currency;

        public Money(decimal amount, string currency) // TODO enum for currency
        {
            if (amount < 0)
            {
                throw new ArgumentException("Money cannot have a negative value");
            }

            Amount = amount;
            _currency = currency;
        }

        public Money Add(Money other)
        {
            if (other._currency != _currency)
            {
                throw new InvalidOperationException("Cannot add money of another currency.");
            }

            return new Money(Amount + other.Amount, _currency);
        }

        public Money Multiply(int by)
        {
            if (by < 1)
            {
                throw new ArgumentException("Cannot multiply money by less than 1");
            }
            return new Money(Amount * by, _currency);
        }

        public Money Sub(Money other)
        {
            if (other._currency != _currency)
            {
                throw new InvalidOperationException("Cannot subtract money of another currency.");
            }

            var newAmount = Math.Max(Amount - other.Amount, 0);

            return new Money(newAmount, _currency);
        }

        public override string ToString() => $"£{Amount:F2}";

        public int CompareTo(Money other)
        {
            if (other._currency != _currency)
            {
                throw new InvalidOperationException("Mismatched currency");
            }

            return Amount.CompareTo(other.Amount);
        }

        // Taken from https://docs.microsoft.com/en-us/dotnet/api/system.icomparable-1?view=net-5.0#examples
        // Define the is greater than operator.
        public static bool operator > (Money operand1, Money operand2)
        {
            return operand1.CompareTo(operand2) > 0;
        }

        // Define the is less than operator.
        public static bool operator < (Money operand1, Money operand2)
        {
            return operand1.CompareTo(operand2) < 0;
        }

        // Define the is greater than or equal to operator.
        public static bool operator >= (Money operand1, Money operand2)
        {
            return operand1.CompareTo(operand2) >= 0;
        }

        // Define the is less than or equal to operator.
        public static bool operator <= (Money operand1, Money operand2)
        {
            return operand1.CompareTo(operand2) <= 0;
        }
    }
}