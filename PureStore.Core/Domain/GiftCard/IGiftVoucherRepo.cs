namespace PureStore.Core.Domain.GiftCard
{
    public interface IGiftVoucherRepo
    {
        public GiftVoucher? FindByCode(Code code);
        public void Save(GiftVoucher voucher);
    }
}