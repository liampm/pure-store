using System;
using System.Text.RegularExpressions;

namespace PureStore.Core.Domain.GiftCard
{
    public class Code
    {
        private readonly string _value;

        public Code(string value)
        {
            if (!Regex.IsMatch(value, @"XXX\-[A-Z]+"))
            {
                throw new ArgumentException("Invalid Gift Code");
            }
            _value = value;
        }

        public override string ToString() => _value;
    }
}