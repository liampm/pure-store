namespace PureStore.Core.Domain.GiftCard
{
    public class GiftVoucher
    {
        public Code Code { get; }
        public Money Amount { get; }

        public GiftVoucher(Code code, Money amount)
        {
            Code = code;
            Amount = amount;
        }
    }
}