using System;
using System.Text.RegularExpressions;

namespace PureStore.Core.Domain.Offer
{
    public class Code
    {
        private readonly string _value;

        public Code(string value)
        {
            if (!Regex.IsMatch(value, @"YYY\-[A-Z]+"))
            {
                throw new ArgumentException("Invalid Offer Code");
            }
            _value = value;
        }
        
        public override string ToString() => _value;
    }
}