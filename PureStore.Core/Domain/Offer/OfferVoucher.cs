using PureStore.Core.Domain.Voucher;

namespace PureStore.Core.Domain.Offer
{
    public class OfferVoucher : IVoucher
    {
        private readonly IVoucher _voucher;
        public Code Code { get; }

        public OfferVoucher(Code code, IVoucher voucher)
        {
            _voucher = voucher;
            Code = code;
        }

        public (Money? saving, string? rejection) CalculateSaving(Basket.Basket basket, string context)
        {
            return _voucher.CalculateSaving(basket, context);
        }
    }
}