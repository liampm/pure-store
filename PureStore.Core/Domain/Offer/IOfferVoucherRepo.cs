namespace PureStore.Core.Domain.Offer
{
    public interface IOfferVoucherRepo
    {
        public OfferVoucher? FindByCode(Code code);
        public void Save(OfferVoucher voucher);
    }
}