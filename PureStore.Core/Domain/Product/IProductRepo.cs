namespace PureStore.Core.Domain.Product
{
    public interface IProductRepo
    {
        public Product? FindBySku(Sku sku);
        public void Save(Product product);
    }
}