using System;
using System.Text.RegularExpressions;

namespace PureStore.Core.Domain.Product
{
    public class Sku
    {
        private readonly string _value;

        public Sku(string value)
        {
            if (!Regex.IsMatch(value, @"SKU\-[0-9]+"))
            {
                throw new ArgumentException("Invalid SKU");
            }

            _value = value;
        }
        public override string ToString() => _value;
    }
}