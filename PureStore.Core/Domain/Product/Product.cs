namespace PureStore.Core.Domain.Product
{
    public record Product(Sku Sku, string Name, Money Price, string Category, bool Discountable = true);
}
