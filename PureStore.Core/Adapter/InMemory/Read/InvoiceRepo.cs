using System;
using System.Linq;
using PureStore.Core.Application.Query.Invoice;

namespace PureStore.Core.Adapter.InMemory.Read
{
    public class InvoiceRepo : IInvoiceRepo
    {
        private readonly Domain.Invoice.IInvoiceRepo _invoiceRepo;

        public InvoiceRepo(Domain.Invoice.IInvoiceRepo invoiceRepo)
        {
            _invoiceRepo = invoiceRepo; // Using other repo for simplicity for now
        }

        public InvoiceDetails? FindById(string id)
        {
            var invoice = _invoiceRepo.FindById(Guid.Parse(id));

            if (invoice == null)
            {
                return null;
            }

            return new InvoiceDetails(
                invoice.Id.ToString(),
                invoice.SubTotal.ToString(),
                invoice.Total.ToString(),
                invoice.GiftVouchers.Select(v => v.Code.ToString()).ToArray(),
                invoice.OfferApplied?.Code.ToString(),
                invoice.OfferUsed,
                invoice.Message
            );
        }
    }
}