using System.Linq;
using PureStore.Core.Application.Query.Basket;

namespace PureStore.Core.Adapter.InMemory.Read
{
    public class BasketRepo : IBasketRepo
    {
        private readonly BasketProvider _basketProvider;

        public BasketRepo(BasketProvider basketProvider)
        {
            _basketProvider = basketProvider; // Using provider for simplicity for now
        }

        public BasketDetails? FindDetails()
        {
            var basket = _basketProvider.Current();

            return new BasketDetails(
                basket.Items().Select(i => (i.ProductName(), i.Count)),
                basket.GiftVouchers.Select(v => v.Code.ToString()).ToArray(),
                basket.OfferVoucher?.Code.ToString()
            );
        }
    }
}