using System.Collections.Generic;
using PureStore.Core.Domain.Offer;

namespace PureStore.Core.Adapter.InMemory
{
    public class OfferVoucherRepo : IOfferVoucherRepo
    {
        private readonly Dictionary<string, OfferVoucher> _vouchers = new();

        public void Save(OfferVoucher voucher)
        {
            _vouchers.Add(voucher.Code.ToString(), voucher);
        }

        public OfferVoucher? FindByCode(Code code)
        {
            return _vouchers.GetValueOrDefault(code.ToString());
        }
    }
}