using System.Collections.Generic;
using PureStore.Core.Domain.Product;

namespace PureStore.Core.Adapter.InMemory
{
    public class ProductRepo : IProductRepo
    {
        private readonly Dictionary<string, Product> _products = new();

        public void Save(Product product)
        {
            _products.Add(product.Sku.ToString(), product);
        }

        public Product? FindBySku(Sku sku)
        {
            return _products.GetValueOrDefault(sku.ToString());
        }
    }
}