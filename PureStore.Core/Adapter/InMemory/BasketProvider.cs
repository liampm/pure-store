using PureStore.Core.Domain.Basket;

namespace PureStore.Core.Adapter.InMemory
{
    public class BasketProvider : IBasketProvider
    {
        private readonly Basket _basket;

        private BasketProvider(Basket basket)
        {
            _basket = basket;
        }

        public static BasketProvider New()
        {
            return new(new Basket());
        }

        public Basket Current()
        {
            return _basket;
        }
    }
}