using System.Collections.Generic;
using PureStore.Core.Domain.GiftCard;

namespace PureStore.Core.Adapter.InMemory
{
    public class GiftVoucherRepo : IGiftVoucherRepo
    {
        private readonly Dictionary<string, GiftVoucher> _vouchers = new();

        public void Save(GiftVoucher voucher)
        {
            _vouchers.Add(voucher.Code.ToString(), voucher);
        }

        public GiftVoucher? FindByCode(Code code)
        {
            return _vouchers.GetValueOrDefault(code.ToString());
        }
    }
}