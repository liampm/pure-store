using System;
using System.Collections.Generic;
using PureStore.Core.Domain.Invoice;

namespace PureStore.Core.Adapter.InMemory
{
    public class InvoiceRepo : IInvoiceRepo
    {
        private readonly Dictionary<string, Invoice> _invoices = new();
        
        public void Save(Invoice invoice)
        {
            _invoices.Add(invoice.Id.ToString(), invoice);
        }

        public Invoice? FindById(Guid id)
        {
            return _invoices.GetValueOrDefault(id.ToString());
        }
    }
}