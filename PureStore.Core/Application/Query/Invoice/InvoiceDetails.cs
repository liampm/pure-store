namespace PureStore.Core.Application.Query.Invoice
{
    public record InvoiceDetails(
        string Id,
        string SubTotal,
        string Total,
        string[] GiftVouchers,
        string? OfferApplied,
        bool OfferUsed,
        string? Message);
}