namespace PureStore.Core.Application.Query.Invoice
{
    public interface IInvoiceRepo
    {
        public InvoiceDetails? FindById(string id);
    }
}