using System.Collections.Generic;

namespace PureStore.Core.Application.Query.Basket
{
    public record BasketDetails(
        IEnumerable<(string name, int count)> Items,
        string[] GiftVouchers,
        string? OfferVoucher
    );
}