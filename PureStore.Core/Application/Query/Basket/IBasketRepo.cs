namespace PureStore.Core.Application.Query.Basket
{
    public interface IBasketRepo
    {
        public BasketDetails? FindDetails();
    }
}