namespace PureStore.Core.Application.Command.Product
{
    public record Remove(string Sku);
}