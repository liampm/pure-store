namespace PureStore.Core.Application.Command.Product
{
    public record Add(string Sku);
}