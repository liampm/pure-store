using System;
using PureStore.Core.Application.Exception;
using PureStore.Core.Domain.Basket;
using PureStore.Core.Domain.Product;

namespace PureStore.Core.Application.Command.Product
{
    public class RemoveHandler
    {
        private readonly IBasketProvider _basketProvider;
        private readonly IProductRepo _productRepo;

        public RemoveHandler(IBasketProvider basketProvider, IProductRepo productRepo)
        {
            _basketProvider = basketProvider;
            _productRepo = productRepo;
        }

        public void Handle(Remove command)
        {
            Sku sku;
            try
            {
                sku = new Sku(command.Sku);
            }
            catch (ArgumentException e)
            {
                throw new BadRequest("Cannot use invalid SKU to remove a product.", e);
            }

            Domain.Product.Product? p = _productRepo.FindBySku(sku);

            if (p == null)
            {
                throw new BadRequest($"Cannot find a product with the SKU {sku}");
            }

            _basketProvider.Current().RemoveProduct(p);
        }

    }
}