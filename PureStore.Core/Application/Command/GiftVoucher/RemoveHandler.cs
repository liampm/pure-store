using System;
using PureStore.Core.Application.Exception;
using PureStore.Core.Domain.Basket;
using PureStore.Core.Domain.GiftCard;

namespace PureStore.Core.Application.Command.GiftVoucher
{
    public class RemoveHandler
    {
        private readonly IBasketProvider _basketProvider;
        private readonly IGiftVoucherRepo _voucherRepo;

        public RemoveHandler(IBasketProvider basketProvider, IGiftVoucherRepo voucherRepo)
        {
            _basketProvider = basketProvider;
            _voucherRepo = voucherRepo;
        }

        public void Handle(Remove command)
        {
            Code code;
            try
            {
                code = new Code(command.Code);
            }
            catch (ArgumentException e)
            {
                throw new BadRequest("Cannot remove a gift voucher with an invalid code.", e);
            }

            Domain.GiftCard.GiftVoucher? v = _voucherRepo.FindByCode(code);

            if (v == null)
            {
                throw new BadRequest($"Cannot find a gift voucher with the code {code}");
            }

            _basketProvider.Current().RemoveGiftVoucher(v);
        }
    }
}