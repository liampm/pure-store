namespace PureStore.Core.Application.Command.GiftVoucher
{
    public record Add(string Code);
}