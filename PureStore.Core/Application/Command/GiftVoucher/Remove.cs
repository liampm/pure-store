namespace PureStore.Core.Application.Command.GiftVoucher
{
    public record Remove(string Code);
}