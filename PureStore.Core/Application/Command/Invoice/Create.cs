namespace PureStore.Core.Application.Command.Invoice
{
    public record Create(string id);
}