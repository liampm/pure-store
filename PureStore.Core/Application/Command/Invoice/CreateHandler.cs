using System;
using PureStore.Core.Application.Exception;
using PureStore.Core.Domain.Basket;
using PureStore.Core.Domain.Invoice;

namespace PureStore.Core.Application.Command.Invoice
{
    public class CreateHandler
    {
        private readonly IBasketProvider _basketProvider;
        private readonly IInvoiceRepo _invoiceRepo;

        public CreateHandler(IBasketProvider basketProvider, IInvoiceRepo invoiceRepo)
        {
            _basketProvider = basketProvider;
            _invoiceRepo = invoiceRepo;
        }

        public void Handle(Create command)
        {
            Guid id;

            try
            {
                id = Guid.Parse(command.id);
            }
            catch (FormatException e)
            {
                throw new BadRequest("Invalid GUID passed for invoice ID.", e);
            }

            _invoiceRepo.Save(Calculator.GenerateInvoice(id, _basketProvider.Current()));
        }
    }
}