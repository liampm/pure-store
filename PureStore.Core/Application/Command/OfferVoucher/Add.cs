namespace PureStore.Core.Application.Command.OfferVoucher
{
    public record Add(string Code);
}