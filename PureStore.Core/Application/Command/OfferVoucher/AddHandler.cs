using System;
using PureStore.Core.Application.Exception;
using PureStore.Core.Domain.Basket;
using PureStore.Core.Domain.Offer;

namespace PureStore.Core.Application.Command.OfferVoucher
{
    public class AddHandler
    {
        private readonly IBasketProvider _basketProvider;
        private readonly IOfferVoucherRepo _voucherRepo;

        public AddHandler(IBasketProvider basketProvider, IOfferVoucherRepo voucherRepo)
        {
            _basketProvider = basketProvider;
            _voucherRepo = voucherRepo;
        }

        public void Handle(Add command)
        {
            Code code;
            try
            {
                code = new Code(command.Code);
            }
            catch (ArgumentException e)
            {
                throw new BadRequest("Cannot add an offer voucher with an invalid code.", e);
            }

            Domain.Offer.OfferVoucher? v = _voucherRepo.FindByCode(code);

            if (v == null)
            {
                throw new BadRequest($"Cannot find an offer voucher with the code {code}");
            }

            _basketProvider.Current().ApplyOffer(v);
        }
    }
}