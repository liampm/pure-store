using System;
using PureStore.Core.Application.Exception;
using PureStore.Core.Domain.Basket;
using PureStore.Core.Domain.Offer;

namespace PureStore.Core.Application.Command.OfferVoucher
{
    public class RemoveHandler
    {
        private readonly IBasketProvider _basketProvider;
        private readonly IOfferVoucherRepo _voucherRepo;

        public RemoveHandler(IBasketProvider basketProvider, IOfferVoucherRepo voucherRepo)
        {
            _basketProvider = basketProvider;
            _voucherRepo = voucherRepo;
        }

        public void Handle(Remove command)
        {
            Code code;
            try
            {
                code = new Code(command.Code);
            }
            catch (ArgumentException e)
            {
                throw new BadRequest("Cannot remove an offer voucher with an invalid code.", e);
            }

            var basket = _basketProvider.Current();

            if (basket.OfferVoucher == null)
            {
                return;
            }

            Domain.Offer.OfferVoucher? v = _voucherRepo.FindByCode(code);

            if (v == null)
            {
                throw new BadRequest($"Cannot find an offer voucher with the code {code}");
            }

            if (basket.OfferVoucher.Code == v.Code)
            {
                basket.RemoveOffer();
            }
        }
    }
}