namespace PureStore.Core.Application.Command.OfferVoucher
{
    public record Remove(string Code);
}