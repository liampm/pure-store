namespace PureStore.Core.Application.Exception
{
    public class NotFound : System.Exception
    {
        public NotFound()
        {
        }
        public NotFound(string message) : base(message)
        {
        }
        public NotFound(string message, System.Exception inner) : base(message, inner)
        {
        }
    }
}