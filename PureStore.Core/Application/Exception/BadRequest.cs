namespace PureStore.Core.Application.Exception
{
    public class BadRequest : System.Exception
    {
        public BadRequest()
        {
        }
        public BadRequest(string message) : base(message)
        {
        }
        public BadRequest(string message, System.Exception inner) : base(message, inner)
        {
        }
    }
}