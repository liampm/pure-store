using System;
using PureStore.Core.Domain;
using Xunit;

namespace PureStore.Tests
{
    public class MoneyTest
    {
        [Fact]
        public void TestCannotBeBelowZero()
        {
            var e = Assert.Throws<ArgumentException>(() => new Money(-1, "GBP"));
            Assert.Same("Money cannot have a negative value", e.Message);
        }

        [Fact]
        public void TestAddition()
        {
            var smallAmount = new Money(1, "GBP");
            var bigAmount = new Money(10, "GBP");

            var result = smallAmount.Add(bigAmount);

            Assert.Equal(new Money(11, "GBP"), result);
        }

        [Fact]
        public void TestAdditionRequiresSameCurrency()
        {
            var smallAmount = new Money(1, "GBP");
            var bigAmount = new Money(10, "USD");

            var e = Assert.Throws<InvalidOperationException>(() => smallAmount.Add(bigAmount));
            Assert.Same("Cannot add money of another currency.", e.Message);
        }

        [Fact]
        public void TestSubtraction()
        {
            var smallAmount = new Money(1, "GBP");
            var bigAmount = new Money(10, "GBP");

            var result = bigAmount.Sub(smallAmount);

            Assert.Equal(new Money(9, "GBP"), result);
        }

        [Fact]
        public void TestSubtractionRequiresSameCurrency()
        {
            var smallAmount = new Money(1, "GBP");
            var bigAmount = new Money(10, "USD");

            var e = Assert.Throws<InvalidOperationException>(() => bigAmount.Sub(smallAmount));
            Assert.Same("Cannot subtract money of another currency.", e.Message);
        }

        [Fact]
        public void TestMultiplication()
        {
            var bigAmount = new Money(10, "GBP");

            var result = bigAmount.Multiply(3);

            Assert.Equal(new Money(30, "GBP"), result);
        }

        [Fact]
        public void TestMoneyCanNeverGoBelowZero()
        {
            var smallAmount = new Money(1, "GBP");
            var bigAmount = new Money(10, "GBP");

            var result = smallAmount.Sub(bigAmount);

            Assert.Equal(new Money(0, "GBP"), result);
        }

        [Fact]
        public void TestFormat()
        {
            var bigAmount = new Money(10, "GBP");

            var result = bigAmount.ToString();

            Assert.Equal("£10.00", result);
        }

        [Fact]
        public void TestLessThan()
        {
            Assert.True(new Money(10, "GBP") < new Money(20, "GBP"));
            Assert.False(new Money(10, "GBP") < new Money(10, "GBP"));
            Assert.False(new Money(20, "GBP") < new Money(10, "GBP"));
        }

        [Fact]
        public void TestLessThanOrEqual()
        {
            Assert.True(new Money(10, "GBP") <= new Money(20, "GBP"));
            Assert.True(new Money(10, "GBP") <= new Money(10, "GBP"));
            Assert.False(new Money(20, "GBP") <= new Money(10, "GBP"));
        }

        [Fact]
        public void TestMoreThan()
        {
            Assert.False(new Money(10, "GBP") > new Money(20, "GBP"));
            Assert.False(new Money(10, "GBP") > new Money(10, "GBP"));
            Assert.True(new Money(20, "GBP") > new Money(10, "GBP"));
        }

        [Fact]
        public void TestMoreThanOrEqual()
        {
            Assert.False(new Money(10, "GBP") >= new Money(20, "GBP"));
            Assert.True(new Money(10, "GBP") >= new Money(10, "GBP"));
            Assert.True(new Money(20, "GBP") >= new Money(10, "GBP"));
        }
    }
}