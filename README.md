# Pure Store

## Overview

This repository is made of 4 projects.

`PureStore.Core` is the heart of the application and is where I have focused most of my efforts.
The code is split into over 3 directories in accordance to
[common DDD practices](https://docs.microsoft.com/en-us/dotnet/architecture/microservices/microservice-ddd-cqrs-patterns/ddd-oriented-microservice#layers-in-ddd-microservices)
(I have called the infrastructure layer "adapter").

`PureStore.Cli` is an unfinished Command Line Interface for a user to interact with the core project.

`PureStore.Specs` is a specification test suite based on the criteria in the task description.
It uses the CLI to drive the tests.

`PureStore.Tests` is the beginning of a unit test suite for the core. If I had more time I would add many more
tests but I hope this small start shows enough.

## Explanation

I have spent a good chunk of the weekend on this - roughly 12 hours - but a lot of that time has been
taken up by the challenges of picking up a new language. This exercise has been very helpful in 
improving my understanding of the C# language and ecosystem.
A sizeable amount of time was spent familiarising myself with BDD and Specflow. I realise it may not
be what you're looking for in the test but John Thrixton had explained how important it is in your 
workflow that I felt I had to give it a try.

I was planning to add an interactive CLI using [Terminal.Gui](https://github.com/migueldeicaza/gui.cs)
if I had more time but I do not think this would demonstrate anything of my ability and I'd just be
investing time in learning the library.

Overall, I think the application gives a good indication of how I like to arrange my code, has clear
separation of responsibilities, and is flexible enough to accommodate new requirements.

## Missing features
- Changeable Currency
- Interactive CLI
- Not expiring vouchers upon use
- Expression language to define vouchers

## Maintenance To Do
- More unit testing.
- Apply a coding standard (find the right tool for this).
- Dockerise
- DI Container - find appropriate library

## Feedback

I have found this to be an enjoyable project and feel that it gives ample opportunity to demonstrate my ability
as a software developer.
I had noticed one issue in the description - The message for Basket 6 should be
"You have not reached the spend threshold for Offer Voucher".