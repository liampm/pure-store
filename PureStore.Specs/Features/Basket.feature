Feature: Product Basket (should this be per feature e.g. gift vouchers, offer vouchers)

    Rules:
     - Gift Voucher:
        - Can be redeemed against the value of a basket.
        - Multiple gift vouchers can be applied to a basket.
        - Gift vouchers can only be redeemed against non gift voucher products.
        - The purchase of a gift voucher does not contribute to the discountable basket total.
    - Offer vouchers:
        - Requires a threshold that needs to be exceeded before a discount can be applied e.g. £5.00 off of baskets over
        £50.
        - Only a single offer voucher can be applied to a basket.
        - Can be applicable to only a subset of products.
    - Offer and Gift vouchers can be used in conjunction.
    - If a customer applies an offer voucher to a basket that will not satisfy the threshold or a customer removes
        item/changes an items quantity resulting in a voucher not being valid then a message will need to be displayed to the customer.

    Scenario: No vouchers
        Given there is a Jumper with the SKU SKU-1 costing £54.65
        And there is a Head Light (Head Gear Category of Product) with the SKU SKU-2 costing £3.50
        When I add 1 SKU-1 to the basket
        And I add 1 SKU-2 to the basket
        Then I should have a sub-total of £58.15
        And no vouchers should be applied
        And I should have a total of £58.15
        And no message is shared
    
    Scenario: One gift voucher
        Given there is a Jumper with the SKU SKU-1 costing £54.65
        And there is a Gloves with the SKU SKU-2 costing £10.50
        And there is a £5.00 Gift Voucher with the code XXX-XXX
        When I add 1 SKU-1 to the basket
        And I add 1 SKU-2 to the basket
        And I add gift voucher XXX-XXX
        Then I should have a sub-total of £65.15
        And 1 x Gift Voucher XXX-XXX should be applied
        And I should have a total of £60.15
        And no message is shared
    
    Scenario: Not applicable offer voucher
        Given there is a Jumper with the SKU SKU-1 costing £26.00
        And there is a Gloves with the SKU SKU-2 costing £25.00
        And there is a £5.00 off Head Gear in baskets over £50.00 Offer Voucher with the code YYY-YYY
        When I add 1 SKU-1 to the basket
        And I add 1 SKU-2 to the basket
        And I add offer voucher YYY-YYY
        Then I should have a sub-total of £51.00
        And Offer Voucher YYY-YYY should be applied
        And I should have a total of £51.00
        And a message saying "There are no products in your basket applicable to Offer Voucher YYY-YYY."
    
    Scenario: Applicable offer voucher
        Given there is a Jumper with the SKU SKU-1 costing £26.00
        And there is a Gloves with the SKU SKU-2 costing £25.00
        And there is a Head Light (Head Gear Category of Product) with the SKU SKU-3 costing £3.50
        And there is a £5.00 off Head Gear in baskets over £50.00 Offer Voucher with the code YYY-YYY
        When I add 1 SKU-1 to the basket
        And I add 1 SKU-2 to the basket
        And I add 1 SKU-3 to the basket
        And I add offer voucher YYY-YYY
        Then I should have a sub-total of £54.50
        And Offer Voucher YYY-YYY should be applied
        And I should have a total of £51.00
        And no message is shared

    Scenario: Applicable offer voucher and gift voucher
        Given there is a Jumper with the SKU SKU-1 costing £26.00
        And there is a Gloves with the SKU SKU-2 costing £25.00
        And there is a £5.00 off baskets over £50.00 Offer Voucher with the code YYY-YYY
        And there is a £5.00 Gift Voucher with the code XXX-XXX
        When I add 1 SKU-1 to the basket
        And I add 1 SKU-2 to the basket
        And I add offer voucher YYY-YYY
        And I add gift voucher XXX-XXX
        Then I should have a sub-total of £51.00
        And Offer Voucher YYY-YYY should be applied
        And 1 x Gift Voucher XXX-XXX should be applied
        And I should have a total of £41.00
        And no message is shared

    Scenario: Offer voucher not applicable because threshold hasn't been met due to eligibility of products
        Given there is a Gloves with the SKU SKU-1 costing £25.00
        And there is a £30 Gift Voucher with the SKU SKU-2 costing £30.00
        And there is a £5.00 off baskets over £50.00 Offer Voucher with the code YYY-YYY
        When I add 1 SKU-1 to the basket
        And I add 1 SKU-2 to the basket
        And I add offer voucher YYY-YYY
        Then I should have a sub-total of £55.00
        And Offer Voucher YYY-YYY should be applied
        And I should have a total of £55.00
        And a message saying "You have not reached the spend threshold for Offer Voucher YYY-YYY. Spend another £25.01 to receive £5.00 discount from your basket total."

    Scenario: Gift voucher greater than total cost
        Given there is a Gloves with the SKU SKU-1 costing £25.00
        And there is a £30.00 Gift Voucher with the code XXX-XXX
        When I add 1 SKU-1 to the basket
        And I add gift voucher XXX-XXX
        Then I should have a sub-total of £25.00
        And 1 x Gift Voucher XXX-XXX should be applied
        And I should have a total of £0.00
        And no message is shared