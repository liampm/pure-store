﻿using PureStore.Cli;
using PureStore.Core;
using PureStore.Core.Adapter.InMemory;
using PureStore.Core.Adapter.InMemory.Read;
using PureStore.Core.Application.Command.Invoice;
using PureStore.Core.Application.Query.Invoice;
using PureStore.Core.Domain;
using PureStore.Core.Domain.Basket;
using PureStore.Core.Domain.GiftCard;
using PureStore.Core.Domain.Offer;
using PureStore.Core.Domain.Product;
using PureStore.Core.Domain.Voucher;
using TechTalk.SpecFlow;
using Xunit;
using Code = PureStore.Core.Domain.Offer.Code;
using InvoiceRepo = PureStore.Core.Adapter.InMemory.InvoiceRepo;

namespace PureStore.Specs.Steps
{
    [Binding]
    public sealed class AppStepDefinitions
    {
        private readonly ScenarioContext _scenarioContext;

        private readonly AppEngine _engine;
        private readonly ProductRepo _productRepo;
        private readonly OfferVoucherRepo _offerVoucherRepo;
        private readonly GiftVoucherRepo _giftVoucherRepo;

        public AppStepDefinitions(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;

            // Set up
            _productRepo = new ProductRepo();
            _offerVoucherRepo = new OfferVoucherRepo();
            _giftVoucherRepo = new GiftVoucherRepo();
            var invoiceRepo = new InvoiceRepo();
            var invoiceReadRepo = new Core.Adapter.InMemory.Read.InvoiceRepo(invoiceRepo);

            var basketProvider = BasketProvider.New();

            _engine = new AppEngine(
                new CommandHandler(
                    new Core.Application.Command.Product.AddHandler(basketProvider, _productRepo),
                    new Core.Application.Command.Product.RemoveHandler(basketProvider, _productRepo),
                    new Core.Application.Command.GiftVoucher.AddHandler(basketProvider, _giftVoucherRepo),
                    new Core.Application.Command.GiftVoucher.RemoveHandler(basketProvider, _giftVoucherRepo),
                    new Core.Application.Command.OfferVoucher.AddHandler(basketProvider, _offerVoucherRepo),
                    new Core.Application.Command.OfferVoucher.RemoveHandler(basketProvider, _offerVoucherRepo),
                    new CreateHandler(basketProvider, invoiceRepo)
                ),
                invoiceReadRepo,
                new BasketRepo(basketProvider)
            );
        }

        [Given(@"there is a ([\w\s\d]+) \((.+) Category of Product\) with the SKU ([^\s]+) costing £(.*)")]
        public void GivenThereIsAProductWithCategory(string itemName, string? category, string sku, decimal cost)
        {
            _productRepo.Save(new Product(new Sku(sku), itemName, new Money(cost, "GBP"), category ?? "Default", true));
        }

        [Given(@"there is a ([\w\s\d]+) with the SKU ([^\s]+) costing £(.*)")]
        public void GivenThereIsAProduct(string itemName, string sku, decimal cost)
        {
            _productRepo.Save(new Product(new Sku(sku), itemName, new Money(cost, "GBP"), "Default", true));
        }

        [Given(@"there is a £([\w\s\d]+) with the SKU ([^\s]+) costing £(.*)")]
        public void GivenThereIsANonDiscountableProduct(string itemName, string sku, decimal cost)
        {
            _productRepo.Save(new Product(new Sku(sku), "£{itemName}", new Money(cost, "GBP"), "Default", false));
        }

        [Given(@"there is a £(.*) Gift Voucher with the code ([^\s]+)")]
        public void GivenThereIsAGiftVoucherWithTheCode(decimal total, string code)
        {
            _giftVoucherRepo.Save((new GiftVoucher(
                new Core.Domain.GiftCard.Code(code),
                new Money(total, "GBP")
            )));
        }

        [Given(@"there is a £5.00 off Head Gear in baskets over £50.00 Offer Voucher with the code ([^\s]+)")]
        public void GivenThereIsAOffHeadGearInBasketsOverOfferVoucherWithTheCode(string code)
        {
            _offerVoucherRepo.Save(new OfferVoucher(
                new Code(code),
                new BasketDependent(
                    new MoneyOff(
                        new Money(5, "GBP"),
                        i => i.Category() == "Head Gear"
                    ),
                    b =>
                    {
                        var discountableTotal = b.DiscountableTotal();
                        var threshold = new Money(50, "GBP");
                        var remainder = threshold.Sub(discountableTotal).Add(new Money(0.01m, "GBP"));
                        return (discountableTotal > threshold)
                            ? null
                            : $"You have not reached the spend threshold for Offer Voucher YYY-YYY. Spend another {remainder.ToString()} to receive £5.00 discount from your basket total.";
                    }
                )
            ));
        }

        [Given(@"there is a £5.00 off baskets over £50.00 Offer Voucher with the code ([^\s]+)")]
        public void GivenThereIsAOffBasketsOverOfferVoucherWithTheCode(string code)
        {
            _offerVoucherRepo.Save(new OfferVoucher(
                new Code(code),
                new BasketDependent(
                    new MoneyOff(
                        new Money(5, "GBP"),
                        _ => true
                    ),
                    b =>
                    {
                        var discountableTotal = b.DiscountableTotal();
                        var threshold = new Money(50, "GBP");
                        var remainder = threshold.Sub(discountableTotal).Add(new Money(0.01m, "GBP"));
                        return (discountableTotal > threshold)
                            ? null
                            : $"You have not reached the spend threshold for Offer Voucher YYY-YYY. Spend another {remainder.ToString()} to receive £5.00 discount from your basket total.";
                    }
                )
            ));
        }

        [When(@"I add (\d+) ([^\s]+) to the basket")]
        public void WhenIAddItemToTheBasket(int amount, string sku)
        {
            _engine.AddProduct(sku); // Do amount times
        }

        [When(@"I add gift voucher ([^\s]+)")]
        public void WhenIAddGiftVoucher(string code)
        {
            _engine.AddGiftVoucher(code);
        }

        [When(@"I add offer voucher ([^\s]+)")]
        public void WhenIAddOfferVoucher(string code)
        {
            _engine.AddOfferVoucher(code);
        }

        [Then(@"I should have a sub-total of (.*)")]
        public void ThenIShouldHaveASubTotalOf(string subTotal)
        {
            var invoice = CreateInvoice();

            Assert.Equal(subTotal, invoice.SubTotal);
        }

        [Then(@"I should have a total of (.*)")]
        public void ThenIShouldHaveATotalOf(string total)
        {
            var invoice = CreateInvoice();

            Assert.Equal(total, invoice.Total);
        }

        [Then(@"no vouchers should be applied")]
        public void ThenNoVouchersAreApplied()
        {
            var invoice = CreateInvoice();

            Assert.Empty(invoice.GiftVouchers);
            Assert.Null(invoice.OfferApplied);
            Assert.False(invoice.OfferUsed);
        }

        [Then(@"Offer Voucher ([^\s]+) should be applied")]
        public void ThenOfferVoucherShouldBeApplied(string code)
        {
            var invoice = CreateInvoice();

            Assert.Equal(code, invoice.OfferApplied);
        }

        [Then(@"(\d) x Gift Voucher ([^\s]+) should be applied")]
        public void ThenGiftVoucherIsApplied(int amount, string code)
        {
            var invoice = CreateInvoice();

            Assert.Contains(code, invoice.GiftVouchers);
        }

        [Then(@"a message saying ""(.*)""")]
        public void ThenAMessageSaying(string message)
        {
            var invoice = CreateInvoice();

            Assert.Equal(message, invoice.Message);
        }

        [Then(@"no message is shared")]
        public void ThenNoMessage()
        {
            var invoice = CreateInvoice();

            Assert.Null(invoice.Message);
        }

        private InvoiceDetails CreateInvoice()
        {
            return _engine.FindInvoice(_engine.CreateInvoice())!;
        }
    }
}