using PureStore.Core.Application.Command.Invoice;
using AddProductHandler = PureStore.Core.Application.Command.Product.AddHandler;
using RemoveProductHandler = PureStore.Core.Application.Command.Product.RemoveHandler;
using AddGiftVoucherHandler = PureStore.Core.Application.Command.GiftVoucher.AddHandler;
using RemoveGiftVoucherHandler = PureStore.Core.Application.Command.GiftVoucher.RemoveHandler;
using AddOfferVoucherHandler = PureStore.Core.Application.Command.OfferVoucher.AddHandler;
using RemoveOfferVoucherHandler = PureStore.Core.Application.Command.OfferVoucher.RemoveHandler;

namespace PureStore.Cli
{
    public class CommandHandler
    {
        private readonly AddProductHandler _addProductHandler;
        private readonly RemoveProductHandler _removeProductHandler;
        private readonly AddGiftVoucherHandler _addGiftVoucherHandler;
        private readonly RemoveGiftVoucherHandler _removeGiftVoucherHandler;
        private readonly AddOfferVoucherHandler _addOfferVoucherHandler;
        private readonly RemoveOfferVoucherHandler _removeOfferVoucherHandler;
        private readonly CreateHandler _createInvoiceHandler;

        public CommandHandler(
                AddProductHandler addProductHandler, 
                RemoveProductHandler removeProductHandler,
                AddGiftVoucherHandler addGiftVoucherHandler,
                RemoveGiftVoucherHandler removeGiftVoucherHandler,
                AddOfferVoucherHandler addOfferVoucherHandler,
                RemoveOfferVoucherHandler removeOfferVoucherHandler,
                CreateHandler createInvoiceHandler
                )
        {
            _addProductHandler = addProductHandler;
            _removeProductHandler = removeProductHandler;
            _addGiftVoucherHandler = addGiftVoucherHandler;
            _removeGiftVoucherHandler = removeGiftVoucherHandler;
            _addOfferVoucherHandler = addOfferVoucherHandler;
            _removeOfferVoucherHandler = removeOfferVoucherHandler;
            _createInvoiceHandler = createInvoiceHandler;
        }

        public void Handle(Core.Application.Command.Product.Add command)
        {
            _addProductHandler.Handle(command);
        }
        public void Handle(Core.Application.Command.Product.Remove command)
        {
            _removeProductHandler.Handle(command);
        }
        public void Handle(Core.Application.Command.GiftVoucher.Add command)
        {
            _addGiftVoucherHandler.Handle(command);
        }
        public void Handle(Core.Application.Command.GiftVoucher.Remove command)
        {
            _removeGiftVoucherHandler.Handle(command);
        }
        public void Handle(Core.Application.Command.OfferVoucher.Add command)
        {
            _addOfferVoucherHandler.Handle(command);
        }
        public void Handle(Core.Application.Command.OfferVoucher.Remove command)
        {
            _removeOfferVoucherHandler.Handle(command);
        }
        public void Handle(Create command)
        {
            _createInvoiceHandler.Handle(command);
        }
    }
}