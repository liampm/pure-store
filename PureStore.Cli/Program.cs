﻿using System;
using PureStore.Core.Adapter.InMemory;
using PureStore.Core.Adapter.InMemory.Read;
using PureStore.Core.Application.Command.Invoice;
using PureStore.Core.Application.Command.Product;
using PureStore.Core.Application.Query.Invoice;
using PureStore.Core.Domain;
using PureStore.Core.Domain.GiftCard;
using PureStore.Core.Domain.Offer;
using PureStore.Core.Domain.Product;
using PureStore.Core.Domain.Voucher;
using Code = PureStore.Core.Domain.Offer.Code;
using InvoiceRepo = PureStore.Core.Adapter.InMemory.InvoiceRepo;

namespace PureStore.Cli
{
    class Program
    {
        static void Main(string[] args)
        {
            var engine = CreateEngine();

            engine.AddProduct("SKU-123");
            engine.AddProduct("SKU-123");
            engine.AddProduct("SKU-123");
            engine.AddProduct("SKU-456");

            engine.AddOfferVoucher("YYY-YYY");
            engine.AddGiftVoucher("XXX-ABC");
            engine.AddGiftVoucher("XXX-DEF");

            var invoiceId = engine.CreateInvoice();

            var invoice = engine.FindInvoice(invoiceId);

            if (invoice == null)
            {
                throw new Exception("Cannot find invoice that has just been created.");
            }

            PrintInvoice(invoice);

            Console.WriteLine(engine.FindBasketDetails());
        }

        private static void PrintInvoice(InvoiceDetails invoice)
        {
            Console.WriteLine($"The subtotal is {invoice.SubTotal}");
            Console.WriteLine($"The total is {invoice.Total}");
            if (invoice.GiftVouchers.Length == 0)
            {
                Console.WriteLine("No Gift Vouchers have been used");
            }
            else
            {
                foreach (var giftVoucher in invoice.GiftVouchers)
                {
                    Console.WriteLine($"Gift voucher {giftVoucher} applied.");
                }
            }

            if (invoice.OfferApplied == null)
            {
                Console.WriteLine("No Offer Voucher has been used");
            }
            else if (invoice.OfferUsed)
            {
                Console.WriteLine($"Offer voucher {invoice.OfferApplied} applied.");
            }

            if (invoice.Message != null)
            {
                Console.WriteLine(invoice.Message);
            }
        }

        private static AppEngine CreateEngine()
        {
            var productRepo = new ProductRepo();
            var offerRepo = new OfferVoucherRepo();
            var giftVoucherRepo = new GiftVoucherRepo();
            var invoiceRepo = new InvoiceRepo();
            var invoiceReadRepo = new PureStore.Core.Adapter.InMemory.Read.InvoiceRepo(invoiceRepo);

            productRepo.Save(new Product(new Sku("SKU-123"), "shoes", new Money(30, "GBP"), "footwear"));
            productRepo.Save(new Product(new Sku("SKU-456"), "hat", new Money(12, "GBP"), "headwear"));

            giftVoucherRepo.Save(new GiftVoucher(new Core.Domain.GiftCard.Code("XXX-ABC"), new Money(10, "GBP")));
            giftVoucherRepo.Save(new GiftVoucher(new Core.Domain.GiftCard.Code("XXX-DEF"), new Money(3, "GBP")));

            offerRepo.Save(new OfferVoucher(
                new Code("YYY-YYY"),
                new BasketDependent(
                    new MoneyOff(
                        new Money(5, "GBP"),
                        i => i.Category() == "headwear"
                    ),
                    b =>
                    {
                        var discountableTotal = b.DiscountableTotal();
                        var threshold = new Money(50, "GBP");
                        var remainder = threshold.Sub(discountableTotal).Add(new Money(0.01m, "GBP"));
                        return (discountableTotal > threshold)
                            ? null
                            : $"You have not reached the spend threshold for Offer Voucher YYY-YYY. Spend another {remainder.ToString()} to receive £5.00 discount from your basket total.";
                    })
            ));

            var basketProvider = BasketProvider.New();

            return new AppEngine(
                new CommandHandler(
                    new AddHandler(basketProvider, productRepo),
                    new RemoveHandler(basketProvider, productRepo),
                    new PureStore.Core.Application.Command.GiftVoucher.AddHandler(basketProvider, giftVoucherRepo),
                    new PureStore.Core.Application.Command.GiftVoucher.RemoveHandler(basketProvider, giftVoucherRepo),
                    new PureStore.Core.Application.Command.OfferVoucher.AddHandler(basketProvider, offerRepo),
                    new PureStore.Core.Application.Command.OfferVoucher.RemoveHandler(basketProvider, offerRepo),
                    new CreateHandler(basketProvider, invoiceRepo)
                ),
                invoiceReadRepo,
                new BasketRepo(basketProvider)
            );
        }
    }
}