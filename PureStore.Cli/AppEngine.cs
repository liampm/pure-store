using System;
using PureStore.Core.Adapter.InMemory.Read;
using PureStore.Core.Application.Command.Invoice;
using PureStore.Core.Application.Query.Basket;
using PureStore.Core.Application.Query.Invoice;

namespace PureStore.Cli
{
    public class AppEngine
    {
        private readonly CommandHandler _commandHandler;
        private readonly InvoiceRepo _invoiceRepo;
        private readonly BasketRepo _basketRepo;

        public AppEngine(CommandHandler commandHandler, InvoiceRepo invoiceRepo, BasketRepo basketRepo)
        {
            _commandHandler = commandHandler;
            _invoiceRepo = invoiceRepo;
            _basketRepo = basketRepo;
        }

        public void AddProduct(string sku)
        {
            _commandHandler.Handle(new Core.Application.Command.Product.Add(sku));
        }

        public void RemoveProduct(string sku)
        {
            _commandHandler.Handle(new Core.Application.Command.Product.Remove(sku));
        }

        public void AddGiftVoucher(string code)
        {
            _commandHandler.Handle(new Core.Application.Command.GiftVoucher.Add(code));
        }

        public void RemoveGiftVoucher(string code)
        {
            _commandHandler.Handle(new Core.Application.Command.GiftVoucher.Remove(code));
        }

        public void AddOfferVoucher(string code)
        {
            _commandHandler.Handle(new Core.Application.Command.OfferVoucher.Add(code));
        }

        public void RemoveOfferVoucher(string code)
        {
            _commandHandler.Handle(new Core.Application.Command.OfferVoucher.Remove(code));
        }

        public string CreateInvoice()
        {
            var id = Guid.NewGuid().ToString();

            _commandHandler.Handle(new Create(id));

            return id;
        }

        public InvoiceDetails? FindInvoice(string id)
        {
            return _invoiceRepo.FindById(id);
        }

        public BasketDetails? FindBasketDetails()
        {
            return _basketRepo.FindDetails();
        }
    }
}